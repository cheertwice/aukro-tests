/**
 * Handling modálního okna s newsletterem aukcí za korunu
 */
Cypress.on('log:added', (obj) => {
    if (obj.consoleProps && obj.consoleProps.URL && obj.consoleProps.URL.includes('isPairedToSubscribedUser')) {
        cy.get('mat-dialog-container[id="mat-dialog-0"] header a').click()
    }
    return false
})

describe('Testy parametrů nabídky', () => {
    /**
     * Přejde na home a přijme cookies
     */
    before( () => {
        // přejít na home
        cy.visit('/')

        // potvrdit cookies
        cy.get('button[id="didomi-notice-agree-button"]').click()
    })

    /**
     * Test body dle zadání v README
     */
    it('Test garance vrácení peněz', () => {
        findSuitableCategory()
        checkCardFlares()
        clickOnAssignedAuction()
        checkAuctionDetail()
    })

    /**
     * Najde kategorii splňující zadání
     */
    const findSuitableCategory = () => {
        // prohledá kategorie, začíná od druhé (první je vánoční dárkový průvodce)
        cy.get('.main-menu auk-top-level-category').then((categories) => {
            recursiveCategorySearch(categories.length, 1);
        })
    }

    /**
     * Rekurzivně prohledá hlavní kategorie na homepage
     *
     * @param categoryCount počet kategorií k prohledání
     * @param categoryActualIndex index aktuálně prohledávané kategorie, běžně začíná na 0
     */
    const recursiveCategorySearch = (categoryCount, categoryActualIndex) => {
        if ((categoryCount - 1) < categoryActualIndex) {
            throw new Error("Invalid recursiveCategorySearch call")
        }

        // klikni na danou polozku v menu
        cy.get('.main-menu auk-top-level-category').eq(categoryActualIndex).click();

        // nastaví filtry
        setUpCategory()

        // vyhodnotí splnění podmínky alespoň 4 aukcí s garancí vrácení peněz, případně přejde na další kategorii
        cy.get('auk-list-card').then((auctions) => {
            if (auctions.length > 4) {
                return;
            }

            const categoryNextIndex = categoryActualIndex + 1

            if ((categoryCount - 1) < categoryNextIndex) {
                throw new Error("V žádné kategorii nebyl nalezen požadovaný počet aukcí.")
            }

            cy.visit('/')
            recursiveCategorySearch(categoryCount, categoryNextIndex);
        })
    }

    /**
     * Nastaví parametry kategorie
     *
     * @TODO: zde je prostor pro rozšíření na testování (nastavení) více parametrů, stačí funkci nastavit případnými parametry a ideálně jí přesunout do nějaké Utility třídy
     */
    const setUpCategory = () => {
        // ověří existenci filtrů
        // čeština není ideální, ale nejsem si jistý, zda tam bude vždy jen jeden filter-checkbox
        cy.get('.filter-sidebar-wrapper auk-filter auk-parameters auk-simple-filter-checkbox')
            .find('.filter-title')
            .should('contain', 'Parametry nabídky')

        // zaklikne garanci vrácení peněz
        // tady jsem nenašel čeho se chytit, toto rozhodně není ideální
        cy.get('.filter-sidebar-wrapper auk-filter auk-parameters auk-simple-filter-checkbox ul li mat-checkbox[id="mat-checkbox-9"]')
            .find('input[type="checkbox"]')
            .check({force: true})

        // ověří správnost url po zakliknutí filtru
        cy.url().should('contain', 'paymentViaAukro=true')
    }

    /**
     * Projde všechny carty aukcí a ověří existenci Garance vrácení peněz
     *
     * @TODO: může kontrolovat jakékoli flares, nastavené ze stejného zdroje jako setUpCategory
     *
     * Zde mi kód několikrát spadl, protože všechny aukce neměly zadanou flare. Přes
     * debug ověřeno, že aplikace byla v požadovaném stavu (zaklikané filtry, správné
     * url za průběhu této funkce).
     */
    const checkCardFlares = () => {
        cy.get('auk-list-card').then((cards) => {
            cards.each((index, card) => {
                cy.wrap(card).find('auk-svg-icon-legacy[id="money-back-guarantee2"]').should('exist')
            })
        })
    }

    /**
     * Vyhodnotí počet karet aukcí na stránce a klikne na příslušnou dle zadání úkolu
     */
    const clickOnAssignedAuction = () => {
        cy.get('auk-list-card').then((cards) => {
            let cardToCheckIndex;
            if (cards.length % 2 !== 0) {
                cardToCheckIndex = Math.floor((cards.length - 1) / 2)
            } else {
                cardToCheckIndex = Math.floor(Math.random() * cards.length);
            }

            cy.wrap(cards).eq(cardToCheckIndex).find('h2[class="product-header"]').click()
            cy.get('auk-item-detail-page').should('be.visible')
        })
    }

    /**
     * Ověří existenci banneru garance vrácení peněz na detailu aukce, následně se
     * pokusí dle zadání úkolu anonymně aukci koupit.
     */
    const checkAuctionDetail = () => {
        // existence banneru
        cy.get('auk-item-detail-premium-banners auk-banner-payment-via-aukro').should('exist')

        // ne-ideální zjištění typu aukce (bid/buyout/obě možnosti)
        cy.get('auk-countdown-panel').invoke('text').then((countdownPanelText) => {
            if (countdownPanelText.includes('Kup teď')) {
                tryAnonymousBuyout()
            }

            if (countdownPanelText.includes('Aukce')) {
                cy.get('auk-item-detail-info').then((detailInfo) => {
                    if (detailInfo.find('auk-item-detail-button-panel').length > 0) {
                        Math.random() < 0.5 ? tryAnonymousBid() : tryAnonymousBuyout()
                    } else {
                        tryAnonymousBid()
                    }
                })

            }
        })
    }

    /**
     * Pokusí se anonymně o "Koupit teď", následně ověří v košíku existence aukce
     * s příslušným názvem
     */
    const tryAnonymousBuyout = () => {
        // načte název aukce
        cy.get('auk-item-detail-title h1').invoke('clone').then((el) => {
            el.children().remove()
            cy.wrap(el.text()).as('title')
        })

        // Kup teď
        cy.get('auk-item-detail-number-input auk-button').click()

        // náhled do košíku
        cy.get('@title').then((title) => {
            cy.get('auk-basket-control').should('contain', title)
        })

    }

    /**
     * Pokusí se anonymně přihodit 20%, dle zadání
     */
    const tryAnonymousBid = () => {
        cy.get('auk-item-detail-main-item-panel-price auk-icon-with-text').invoke('text').then((currentPriceText) => {
            // načtení ceny
            let currentPrice = parseInt(currentPriceText.replace(/\D/g, ''))

            // výpočet ceny k příhozu
            let priceToBid = (Math.floor(currentPrice * 1.2)).toString()

            // zapsání příhozu
            cy.get('auk-item-detail-number-input input').clear().type(priceToBid)

            // příhoz
            cy.get('auk-item-detail-number-input auk-button').click()

            // mělo by vyskočit přihlašovací okno
            cy.get('auk-authenticate-wrapper').should('be.visible')
        })
    }
})



