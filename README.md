# QA ZADÁNÍ

**Vytvořte automatizovaný test dle následujícího scénáře**

Vyjděte ze stránky https://aukro.cz/

Projděte kategorie nejvyšší úrovně v levém menu (Sběratelství, Starožitnosti a
umění, ...) než naleznete první vyhovující kategorii (viz body níže)
V každé kategorii využijte filtrování "Parametry nabídky", kde zvolte možnost
"Garance vrácení peněz"
Pří průchodu kategorií nalezněte první z nich, která obsahuje VÍCE NEŽ 4 nabídky
(tzn. 5 a více nabídek)

Pro všechny nabídky na stránce proveďte následující: <br>
● Zkontrolujte, že všechny nabídky na stránce mají u sebe zlatý odznáček
"Garance vrácení peněz"<br>
● Klikněte na PROSTŘEDNÍ nabídku z výpisu, čímž přejdete na detail nabídky
(pokud je seznam nabídek sudý, zvolte nabídku náhodně)<br>
● Na detailu nabídky proveďte následující:<br>
● Zkontrolujte, že nabídka má zlatý odznáček "Garance vrácení peněz" v boxíku
dopravy a platby (napravo dole vedle hlavního obrázku)<br>
● Zjistěte, jestli se jedná o "Kup teď" nabídku (nabídka typická pro eshopy, ihned si
ji vložím do košíku, zaplatím a je má) nebo o aukci. Způsob zjištění je čistě na vás.<br>
● Pokud se jedná o "Kup teď" nabídku, vložím ji do košíku a zkontroluji, jestli se do
košíku správně vložila (lze využít košík v hlavičce)<br>
● Pokud se jedná o "Aukci", přihodím aktuální nabízenou hodnotu + 20 %. Ověřím,
že jsem byl přesměrován na přihlašovací stránku<br>
● Pokud se jedná o nabídku, která umožňuje jak "Kup teď" tak aukci, vyberu si
scénář testu náhodně (tzn. že test náhodně zvolí jestli protestuje "Kup teď" nebo
"Aukci")

**Věci, na které si dát pozor**

Při první návštěvě Aukra se zobrazí souhlas s cookies, tuto situaci je třeba
handlovat, tak aby se zbytek testu choval vždy správně. V kategorii po čase vyskočí modální okno s nabídkou newsletteru aukcí za 1 korunu. Test by tedy měl
obsahovat handling situace, kdy se modální okno zobrazí. Žádaným chováním je
ZAVŘENÍ daného modálního okna za využití křížku v pravém horním rohu.

**Poznámky k realizaci**

K implementaci využijte cypress (https://www.cypress.io/)

Pokud selže jediná kontrola, selže celý test s vypovídající chybovou hláškou
Navrhněte strukturu testu tak, aby bylo co nejsnazší testovací scénář obohatit či
vytvořit další podobný.

Preferovaným jazykem pro realizaci je Typescript, využití vanilla javascriptu je také
možné.
V testu složitější konstrukce komentujte tak, aby bylo zjevné co daná část kódu
dělá bez nutnosti bližší investigace.
Výsledný projekt s testem nahrajte na některé gitové úložiště (github, gitlab,
bitbucket), nastavte repozitář jako veřejný (či veřejný pouze přes odkaz) a odkaz
na něj nám zašlete.

#### Pripadne problemy a jejich reseni
**404** <br>
stačí udělat na jakémkoliv win pc tohle s projektem z jeho repozitáře:
1) rozbalení
2) instalace dependecies z locku - npm ci
3) nastavení práva na spouštění skriptů - Set-ExecutionPolicy - ExecutionPolicy
Unrestricted - Scope Process
4) smazání všeho v AppData/Roaming/cypress
5) spuštění cypressu - ./node_modules/.bin/cypress open
- pokud neměl ještě nějaký jiný problémy, tak to může zkusit a vyvíjet dál

**403 při návštěvě jakékoliv stránky** <br>
Do souboru C:\Windows\System 32 \Drivers\etc\hosts přidat dva záznamy: <br>
185.175.85.226 aukro.cz <br>
185.175.85.226 backend.aukro.cz


